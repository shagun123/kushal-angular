import {Injectable} from '@angular/core';
import {HttpService} from './http-service';
import {map} from 'rxjs/operators';
import {User} from '../models/user';
import {Observable} from 'rxjs';
import {Post} from '../models/post';

@Injectable()
export class ApiService {
  constructor(private httpService: HttpService) {
  }

  getUsers(): Observable<User[]> {
    return this.httpService.get('/users').pipe(map(data => data as User[]));
  }

  getPostList(): Observable<Post[]> {
    const data: Post[] = [{
      title: 'post 1', id: 1,
      comments: [{id: 11, description: 'comment 1'}]
    }, {
      title: 'post 2', id: 2,
      comments: [{id: 12, description: 'comment 2'}]
    }];
    return new Observable(observer => {
      observer.next(data);
    });
  }
}

