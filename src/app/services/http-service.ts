import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AlertService} from './alert-service';
import {Observable, throwError as observableThrowError} from 'rxjs';
import {catchError} from 'rxjs/operators';

@Injectable()

export class HttpService {
  AUTH_TOKEN = 'auth_token';
  private baseUrl = 'https://jsonplaceholder.typicode.com';

  constructor(private httpClient: HttpClient, private alertService: AlertService) {
  }

  get(url, params?: any): Observable<any> {
    const data = {
      headers: this.getAuthHeader(),
      params
    };
    return this.httpClient.get(this.baseUrl + url, data)
      .pipe(catchError(this.handleError.bind(this)));
  }

  private getAuthHeader(): { [header: string]: string | string[]; } {
    return {
      Authorization: `Bearer ${localStorage.getItem(this.AUTH_TOKEN)}`
    };
  }

  private handleError(response: any) {
    const error = response.error;
    const keys = Object.keys(error);
    const key = keys[0];
    let message = error[key];
    if (response.status === 401) {
      localStorage.removeItem(this.AUTH_TOKEN);
      return window.location.href = '';
    }

    if (error[key] instanceof Array) {
      message = error[key][0];
    }

    if (key === 'isTrusted') {
      message = 'Please check your internet connection !';
    } else {
      message = key + ' : ' + message;
    }

    this.alertService.error(message);

    return observableThrowError({messages: message, error});
  }
}
