import {Action} from './action';
import {User} from '../models/user';

export const USER_LIST_REQUEST = 'user list request';
export const USER_LIST_SUCCESS = 'user list success';
export const USER_LIST_FAILED = 'user list failed';
export const USER_UPDATE = 'user update action';
export const USER_DELETE = 'user delete action';

export class UserListRequestAction implements Action {
  readonly type = USER_LIST_REQUEST;
}

export class UserUpdateAction implements Action {
  readonly type = USER_UPDATE;

  constructor(public payload: { data: User }) {
  }
}

export class UserListSuccessAction implements Action {
  readonly type = USER_LIST_SUCCESS;

  constructor(public payload: { data: User[] }) {
  }
}

export class UserListFailed implements Action {
  readonly type = USER_LIST_FAILED;
}

export class UserDeleteAction implements Action {
  readonly type = USER_DELETE;

  constructor(public payload: { data: number }) {
  }
}
