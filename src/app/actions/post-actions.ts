import {Action} from './action';
import {Post} from '../models/post';
import {Comment} from '../models/post';

export enum PostActions {
  POST_LIST_REQUEST = 'post list request',
  POST_LIST_SUCCESS = 'post list success',
  POST_ADDED = 'post added success',
  COMMENT_ADD_ACTION = 'add comment',
  COMMENT_REMOVE_ACTION = 'remove comment',
  COMMENT_UPDATE_ACTION = 'update comment'
}

export class PostListRequestAction implements Action {
  readonly type = PostActions.POST_LIST_REQUEST;
}

export class PostListSuccessAction implements Action {
  readonly type = PostActions.POST_LIST_SUCCESS;

  constructor(public payload: { data: Post[] }) {
  }
}

export class PostAddedAction implements Action {
  readonly type = PostActions.POST_ADDED;

  constructor(public payload: { data: Post }) {
  }
}


export class CommentAddAction implements Action {
  readonly type = PostActions.COMMENT_ADD_ACTION;

  constructor(public payload: { data: Comment, postId: number }) {
  }
}

export class CommentDeleteAction implements Action {
  readonly type = PostActions.COMMENT_REMOVE_ACTION;

  constructor(public payload: { id: number, postId: number }) {
  }
}

export class CommentUpdateAction implements Action {
  readonly type = PostActions.COMMENT_UPDATE_ACTION;

  constructor(public payload: { data: Comment, postId: number }) {
  }
}





