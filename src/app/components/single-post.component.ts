import {Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {map, switchMap} from 'rxjs/operators';
import {YoutubeRepository} from '../repository/youtube-repository.service';

@Component({
  selector: 'youtube-single-post',
  template: `
    <h1>this is a single post</h1>
  `,
  styles: [``]
})

export class SinglePostComponent {

  constructor(private activatedRoute: ActivatedRoute, private youtubeRepository: YoutubeRepository) {
    const post$ = this.activatedRoute.params.pipe(map((data: any) => data.id),
      switchMap((id) => {
        return this.youtubeRepository.getSinglePost(id);
      }));
    post$.subscribe(data => {
      console.log(data);
    });
  }
}
