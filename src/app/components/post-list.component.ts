import {Component, Input} from '@angular/core';
import {Post} from '../models/post';

@Component({
  selector: 'youtube-post-list',
  template: `
    <div fxLayoutAlign="center stretch" fxLayoutWrap fxLayoutGap="30px">
      <youtube-post-item *ngFor="let post of this.posts" [post]="post"></youtube-post-item>
      <span class="card"></span>
      <span class="card"></span>
      <span class="card"></span>
    </div>
  `,
  styles: [``]
})

export class PostListComponent {
  @Input() posts: Post[];

  constructor() {
  }
}
