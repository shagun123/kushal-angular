import {Component, Input} from '@angular/core';
import {User} from '../models/user';

@Component({
  selector: 'youtube-user-list',
  template: `
    <div fxLayoutAlign="center stretch" fxLayoutWrap fxLayoutGap="30px">
      <youtube-user-item *ngFor="let user of this.userList" [user]="user"></youtube-user-item>
      <span class="card"></span>
      <span class="card"></span>
      <span class="card"></span>
    </div>
  `,
  styles: [`
    .card, youtube-user-item {
      width: 250px;
      height: 250px;
      margin-bottom: 30px;
    }
  `]
})

export class UserListComponent {
  @Input() userList: User[];

  constructor() {
  }
}
