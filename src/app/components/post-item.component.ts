import {Component, Input} from '@angular/core';
import {Post} from '../models/post';
import {Router} from '@angular/router';

@Component({
  selector: 'youtube-post-item',
  template: `
    <mat-card (click)="this.route()" style="margin-bottom: 30px;" fxLayout="column" fxLayoutAlign="start stretch">
      <mat-card-title>{{this.post.title}}</mat-card-title>
    </mat-card>
  `,
  styles: [``]
})

export class PostItemComponent {
  @Input() post: Post;

  constructor(private router: Router) {
  }

  route() {
    this.router.navigate(['post', this.post.id]);
  }
}
