import * as fromUser from './user-reducer';
import * as fromPost from './post-reducer';
import {ActionReducerMap, createSelector} from '@ngrx/store';

export interface RootReducerState {
  users: fromUser.UserReducerState;
  postList: fromPost.PostReducerState;
}

// combination of all reducer
export const rootReducer: ActionReducerMap<RootReducerState> = {
  users: fromUser.userReducer,
  postList: fromPost.postReducer
};

// state
export const getUserState = (state: RootReducerState) => state.users;

// selector
export const getUserLoading = createSelector(getUserState, fromUser.getLoading);
export const getUserLoaded = createSelector(getUserState, fromUser.getLoaded);
export const getUsersData = createSelector(getUserState, fromUser.getUsers);


export const getPostState = (state: RootReducerState) => state.postList;

export const getPostLoading = createSelector(getPostState, fromPost.getLoading);
export const getPostLoaded = createSelector(getPostState, fromPost.getLoaded);
export const getPostEntities = createSelector(getPostState, fromPost.getEntities);
export const getPostIds = createSelector(getPostState, fromPost.getIds);
export const getPostData = createSelector(getPostState, fromPost.getPostList);
export const getSinglePost = (state: RootReducerState, id: number) => {
  const entities = getPostEntities(state);
  return entities[id];
};
