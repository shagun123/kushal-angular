import {Post} from '../models/post';
import {Action} from '../actions/action';
import {PostActions} from '../actions/post-actions';
import {ReduxUtils} from '../utils/redux-utils';
import {createSelector} from '@ngrx/store';

export interface PostReducerState {
  ids: number[];
  entities: { [id: number]: Post };
  loading: boolean;
  loaded: boolean;
}

export const initialState: PostReducerState = {
  ids: [],
  entities: {},
  loaded: false,
  loading: false
};

export function postReducer(state = initialState, action: Action): PostReducerState {
  switch (action.type) {
    case PostActions.POST_LIST_REQUEST: {
      return {...state, loading: true};
    }
    case PostActions.POST_LIST_SUCCESS: {
      const normalizedPost = ReduxUtils.normalize(action.payload.data);
      const ids = action.payload.data.map(data => data.id);
      const newIds = ReduxUtils.filterDuplicateIds([...state.ids, ...ids]);
      const newEntities = {...state.entities, ...normalizedPost};
      return {...state, ...{ids: newIds, entities: newEntities, loaded: true, loading: false}};
    }
    case PostActions.COMMENT_ADD_ACTION: {
      const postId = action.payload.postId;
      const comment = action.payload.data;
      const oldPost = JSON.parse(JSON.stringify(state.entities[postId]));
      oldPost.comments.push(comment);
      const obj = {[postId]: oldPost};
      const entities = {...state.entities, ...obj};
      return {...state, entities};
    }
    case PostActions.COMMENT_UPDATE_ACTION: {
      const postId = action.payload.postId;
      const comment = action.payload.data;
      const oldPost = JSON.parse(JSON.stringify(state.entities[postId]));
      const postWithoutComment = oldPost.comments.filter(data => data.id !== comment.id);
      postWithoutComment.push(comment);
      oldPost.comments = postWithoutComment;
      const obj = {[oldPost.id]: oldPost};
      const newEntities = {...state.entities, ...obj};
      return {...state, entities: newEntities};
    }
    case PostActions.COMMENT_REMOVE_ACTION: {
      const postId = action.payload.postId;
      const commentId = action.payload.id;
      const oldPost = JSON.parse(JSON.stringify(state.entities[postId]));
      oldPost.comments = oldPost.comments.filter(data => data.id !== commentId);
      const obj = {[oldPost.id]: oldPost};
      const newEntities = {...state.entities, ...obj};
      return {...state, entities: newEntities};
    }
    case PostActions.POST_ADDED: {
      const postData = action.payload.data;
      const obj = {[postData.id]: postData};
      const newIds = [...state.ids, postData.id];
      const newEntities = {...state.entities, ...obj};
      return {...state, ...{entities: newEntities, ids: newIds}};
    }
    default: {
      return state;
    }
  }
}

export const getLoading = (state: PostReducerState) => state.loading;
export const getLoaded = (state: PostReducerState) => state.loaded;
export const getEntities = (state: PostReducerState) => state.entities;
export const getIds = (state: PostReducerState) => state.ids;
export const getPostList = createSelector(getEntities, getIds, (entity, ids) => {
  return ids.map(id => {
    return entity[id];
  });
});
