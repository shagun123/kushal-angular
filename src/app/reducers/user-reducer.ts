import {User} from '../models/user';
import {Action} from '../actions/action';
import {USER_DELETE, USER_LIST_FAILED, USER_LIST_REQUEST, USER_LIST_SUCCESS, USER_UPDATE} from '../actions/user-actions';
import {ReduxUtils} from '../utils/redux-utils';
import {createSelector} from '@ngrx/store';


export interface UserReducerState {
  ids: number[];
  entities: { [id: number]: User };
  loading: boolean;
  loaded: boolean;
}

export const initialState: UserReducerState = {
  ids: [],
  entities: {},
  loaded: false,
  loading: false
};
// it will receive state and action according to the action it will
// return a new state;
// state is immutable;
export function userReducer(state = initialState, action: Action): UserReducerState {
  console.log(action.type);
  switch (action.type) {
    case USER_LIST_REQUEST: {
      return {...state, loading: true};
    }
    case USER_LIST_SUCCESS: {
      const users = ReduxUtils.normalize(action.payload.data);
      const ids = action.payload.data.map(data => data.id);
      const newIds = ReduxUtils.filterDuplicateIds([...state.ids, ...ids]);
      const newEntities = {...state.entities, ...users};
      return {...state, ...{ids: newIds, entities: newEntities, loaded: true, loading: false}};
    }
    case USER_UPDATE: {
      // {id:{}};
      const user = {[action.payload.data.id]: action.payload.data};
      const newEntities = {...state.entities, ...user};
      return {...state, ...{entities: newEntities}};
    }
    case USER_DELETE: {
      const id = action.payload.data;
      const newIds = state.ids.filter(data => data !== id);
      const newEntities = ReduxUtils.removeKey(state.entities, id);
      return {...state, ...{entities: newEntities, ids: newIds}};
    }
    case USER_LIST_FAILED: {
      return {...state, loading: false};
    }
    default: {
      return state;
    }
  }
}

// selectors - to receive or listen values from reducer

export const getLoading = (state: UserReducerState) => state.loading;
export const getLoaded = (state: UserReducerState) => state.loaded;
export const getEntities = (state: UserReducerState) => state.entities;
export const getIds = (state: UserReducerState) => state.ids;
export const getUsers = createSelector(getEntities, getIds, (entity, ids) => {
  return ids.map(id => {
    return entity[id];
  });
});
