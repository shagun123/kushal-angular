import {NgModule} from '@angular/core';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatButtonModule} from '@angular/material/button';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatCardModule} from '@angular/material/card';


const modules = [
  MatSnackBarModule,
  MatToolbarModule,
  MatButtonModule,
  MatCardModule
];

@NgModule({
  imports: modules,
  exports: modules
})

export class MaterialModule {
}
