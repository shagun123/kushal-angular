import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {
  getPostData,
  getPostLoaded,
  getPostLoading,
  getSinglePost,
  getUserLoaded,
  getUserLoading,
  getUsersData,
  RootReducerState
} from '../reducers';
import {combineLatest, Observable} from 'rxjs';
import {UserDeleteAction, UserListFailed, UserListRequestAction, UserListSuccessAction, UserUpdateAction} from '../actions/user-actions';
import {ApiService} from '../services/api-service';
import {User} from '../models/user';
import {
  CommentAddAction,
  CommentDeleteAction,
  CommentUpdateAction, PostAddedAction,
  PostListRequestAction,
  PostListSuccessAction
} from '../actions/post-actions';
import {Post} from '../models/post';
import {Comment} from '../models/post';

@Injectable()
export class YoutubeRepository {
  constructor(private store: Store<RootReducerState>, private apiService: ApiService) {
  }

  fetchUsers(force = false): [Observable<User[]>, Observable<boolean>] {
    const loading$ = this.store.select(getUserLoading);
    const loaded$ = this.store.select(getUserLoaded);
    const user$ = this.store.select(getUsersData);
    combineLatest([loaded$, loading$]).subscribe((data) => {
      if (!data[0] && !data[1] || force) {
        this.store.dispatch(new UserListRequestAction());
        this.apiService.getUsers().subscribe((users) => {
          this.store.dispatch(new UserListSuccessAction({data: users}));
        }, () => {
          this.store.dispatch(new UserListFailed());
        });
      }
    }, (error) => {
      this.store.dispatch(new UserListFailed());
    });
    return [user$, loading$];
  }

  fetchPostList(force = false): [Observable<Post[]>, Observable<boolean>] {
    const loading$ = this.store.select(getPostLoading);
    const loaded$ = this.store.select(getPostLoaded);
    const post$ = this.store.select(getPostData);
    combineLatest([loading$, loaded$]).subscribe((data) => {
      if (!data[0] && !data[1] || force) {
        this.store.dispatch(new PostListRequestAction());
        this.apiService.getPostList().subscribe(res => {
          this.store.dispatch(new PostListSuccessAction({data: res}));
        });
      }
    });
    return [post$, loading$];
  }

  updateUser() {
    const user: User = {
      id: 1,
      name: 'shagun',
      email: 'shagungarg2010@gmail.com',
      username: 'shagun@!@#',
      address: {
        street: 'kulas light',
        suite: 'abc',
        city: 'delhi',
        zipcode: '20110',
        geo: {
          lat: '20',
          lng: '12'
        }
      }
    };
    this.store.dispatch(new UserUpdateAction({data: user}));
  }

  deleteUser() {
    this.store.dispatch(new UserDeleteAction({data: 1}));
  }

  addComment() {
    const comment: Comment = {description: 'adsds', id: 15};
    this.store.dispatch(new CommentAddAction({data: comment, postId: 1}));
  }

  updateComment() {
    const comment: Comment = {description: 'this is an updated comment', id: 15};
    this.store.dispatch(new CommentUpdateAction({data: comment, postId: 1}));
  }

  deleteComment() {
    this.store.dispatch(new CommentDeleteAction({id: 15, postId: 1}));
  }

  getSinglePost(id: number) {
    const singlePost$ = this.store.select((state => getSinglePost(state, id)));
    singlePost$.subscribe(data => {
      if (!data) {
        // api call
        const postData = {
          title: 'post 1', id: 1,
          comments: [{id: 11, description: 'comment 1'}]
        };
        this.store.dispatch(new PostAddedAction({data: postData}));
      }
    }).unsubscribe();
    return singlePost$;
  }
}
