export class ReduxUtils {
  static normalize(entityArray: Entity[]): any {
    return entityArray.reduce((entities: { [id: number]: Entity }, entity: Entity) => {
      return {
        ...entities, ...{
          [entity.id]: entity
        }
      };
    }, {});
  }

  static removeKey(obj, deleteKey) {
    return Object.keys(obj)
      .filter(key => {
        // tslint:disable-next-line:radix
        return parseInt(key) !== deleteKey;
      })
      .reduce((result, current) => {
        result[current] = obj[current];
        return result;
      }, {});
  }

  static filterDuplicateIds(ids: number[]) {
    return ids.filter((elem, index, self) => index === self.indexOf(elem));
  }
}

interface Entity {
  id: number;
}
