import {Component, OnDestroy, OnInit} from '@angular/core';
import {YoutubeRepository} from '../repository/youtube-repository.service';
import {User} from '../models/user';
import {takeWhile} from 'rxjs/operators';
import {Post} from '../models/post';

@Component({
  selector: 'youtube-post',
  template: `
    <div fxLayout="column" fxLayoutAlign="start center" fxLayoutGap="20px">
      <youtube-post-list fxFlex="90%" [posts]="this.posts"></youtube-post-list>
      <button (click)="this.addComment()" mat-button color="primary">Add Comment</button>
      <button (click)="this.updateComment()" mat-button color="accent">Update Comment</button>
      <button (click)="this.deleteComment()" mat-button color="accent">Delete Comment</button>
    </div>
  `,
  styles: [``]
})

export class PostComponent implements OnInit, OnDestroy {
  posts: Post[] = [];
  isAlive = true;

  ngOnInit() {
    this.fetchData();
  }

  ngOnDestroy() {
    this.isAlive = false;
  }

  fetchData() {
    const post$ = this.youtubeRepository.fetchPostList()[0];
    post$.pipe(takeWhile(() => this.isAlive)).subscribe((data) => {
      this.posts = data;
      console.log('updated data', data);
    });
  }

  addComment() {
    this.youtubeRepository.addComment();
  }

  updateComment() {
    this.youtubeRepository.updateComment();
  }
  deleteComment(){
    this.youtubeRepository.deleteComment();
  }

  constructor(private youtubeRepository: YoutubeRepository) {
  }
}
