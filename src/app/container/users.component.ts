import {Component, OnDestroy, OnInit} from '@angular/core';
import {User} from '../models/user';
import {takeWhile} from 'rxjs/operators';
import {YoutubeRepository} from '../repository/youtube-repository.service';
import {ReduxUtils} from '../utils/redux-utils';

@Component({
  selector: 'youtube-users',
  template: `
    <div fxLayout="column" fxLayoutAlign="start center" fxLayoutGap="20px">
      <youtube-user-list fxFlex="90%" [userList]="users"></youtube-user-list>
      <button (click)="updateUser()" mat-button color="primary">UPDATE MY USER</button>
      <button (click)="deleteUser()" mat-button color="accent">Delete MY USER</button>
    </div>
  `,
  styles: [``]
})

export class UsersComponent implements OnInit, OnDestroy {
  users: User[] = [];
  isAlive = true;

  constructor(private youtubeRepository: YoutubeRepository) {
  }

  ngOnInit() {
    this.fetchData();
  }

  ngOnDestroy() {
    this.isAlive = false;
  }

  fetchData() {
    this.youtubeRepository.fetchUsers()[0].pipe(takeWhile(() => this.isAlive)).subscribe(data => {
      this.users = data;
    });
  }

  updateUser() {
    this.youtubeRepository.updateUser();
  }

  deleteUser() {
    this.youtubeRepository.deleteUser();
  }

}

// ngrx store -> reducer contains action and state
// whenever an action is dispatched reducer will return new state

// store -> root state which contains all reducers -> it contains selectors also


// combine latest
// observable A , observable B
