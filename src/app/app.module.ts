import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FlexLayoutModule, FlexModule} from '@angular/flex-layout';
import {MaterialModule} from './material.module';
import {HttpClientModule} from '@angular/common/http';
import {AlertService} from './services/alert-service';
import {HeaderComponent} from './components/layout/header.component';
import {YoutubeLayoutComponent} from './components/layout/youtube-layout.component';
import {DashboardComponent} from './components/layout/dashboard.component';
import {UsersComponent} from './container/users.component';
import {PostComponent} from './container/post.component';
import {HttpService} from './services/http-service';
import {ApiService} from './services/api-service';
import {UserItemComponent} from './components/user-item.component';
import {UserListComponent} from './components/user-list.component';
import {StoreModule} from '@ngrx/store';
import {rootReducer} from './reducers';
import {YoutubeRepository} from './repository/youtube-repository.service';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {environment} from '../environments/environment';
import {PostListComponent} from './components/post-list.component';
import {PostItemComponent} from './components/post-item.component';
import {SinglePostComponent} from './components/single-post.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    YoutubeLayoutComponent,
    DashboardComponent,
    UsersComponent,
    PostComponent,
    UserItemComponent,
    UserListComponent,
    PostListComponent,
    PostItemComponent,
    SinglePostComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    FlexModule,
    MaterialModule,
    HttpClientModule,
    StoreModule.forRoot(rootReducer),
    StoreDevtoolsModule.instrument({maxAge: 25, logOnly: environment.production})
  ],
  providers: [AlertService, HttpService, ApiService, YoutubeRepository],
  bootstrap: [AppComponent]
})
export class AppModule {
}
